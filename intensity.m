function intRes = intensity(G, x)
    %Fungsi menjumlahkan RGB pada Gambar senilai x
    %Fungis menaikkan/menurunkan intensitas
    intRes = uint8(G(:,:,:) + x);
end