function zoomedOut = zoomOut(G, x)
    zoomedOut = zeros(size(G,1) / x, size(G,2) / x, 3);
    t1 = 1, t2 = 1;
    for i = 1:size(zoomedOut,1)
        for j = 1:size(zoomedOut,2)
            zoomedOut(i, j, :) = G(t1, t2, :);
            t2 = t2 + x;
        end
        t1 = t1 + x;
        t2 = 1;
    end
    zoomedOut = uint8(zoomedOut);
end