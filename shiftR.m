function shiftedR = shiftR(G, x)
    shiftedR = zeros(size(G,1), size(G,2), 3);
    shiftedR(:, x+1:end, :) = G(:, 1:end-x, :);
    shiftedR = uint8(shiftedR)
end