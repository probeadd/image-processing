function rotated90 = rotation90(G)
    rotated90 = zeros(size(G,2), size(G,1), 3);
    k = size(G,1);
    for i = 1:size(G,1)
        for j = 1:size(G,2)
            rotated90(j, k, :) = G(i, j, :);
        end
        k = k - 1;
    end
    rotated90 = uint8(rotated90);
end
    