function zoomedIn = zoomIn(G, x)
    zoomedIn = zeros(x * size(G,1), x * size(G,2), 3);
    t1 = 1, t2 = 1;
    for i = 1:size(G,1)
        for j = 1:size(G,2)
            for k = 1:x
                for l = 1:x
                    zoomedIn(t1, t2, :) = G(i, j, :);
                    zoomedIn(t1, t2 + l, :) = G(i, j, :);
                    zoomedIn(t1 + k, t2, :) = G(i, j, :);
                    zoomedIn(t1 + k, t2 + l, :) = G(i, j, :);
                end
            end
            t2 = t2 + x;
        end
        t1 = t1 + x;
        t2 = 1;
    end
    zoomedIn = uint8(zoomedIn)
end