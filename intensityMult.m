function intMultRes = intensityMult(G, x)
    %Fungsi mengalikan RGB pada Gambar senilai x
    %Fungis menajamkan gambar
    intMultRes = uint8(G(:,:,:) * x);
end