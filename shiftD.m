function shiftedD = shiftD(G, x)
    shiftedD = zeros(size(G,1), size(G,2), 3);
    shiftedD(x+1:end, :, :) = G(1:end-x, :, :);
    shiftedD = uint8(shiftedD)
end