function rotatedMin90 = rotationMin90(G)
    rotatedMin90 = zeros(size(G,2), size(G,1), 3);
    k = 1;
    for i = 1:size(G,1)
        for j = 1:size(G,2)
            rotatedMin90(j, k, :) = G(i, j, :);
        end
        k = k + 1;
    end
    rotatedMin90 = uint8(rotatedMin90);
end