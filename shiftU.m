function shiftedU = shiftU(G, x)
    shiftedU = zeros(size(G,1), size(G,2), 3);
    shiftedU(1:end-x, :, :) = G(1+x:end, :, :);
    shiftedU = uint8(shiftedU)
end