function rotated180 = rotation180(G)
    rotated180 = rotationMin90(rotation90(G));
end    