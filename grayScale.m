function gray = grayScale(G)
    %Fungsi akan menghasilkan gambar grayscale, koefisien disesuaikan
    gray = uint8(.2989 * G(:,:,1) + .5870 * G(:,:,2) + .1140 * G(:,:,3));
end