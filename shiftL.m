function shiftedL = shiftL(G, x)
    shiftedL = zeros(size(G,1), size(G,2), 3);
    shiftedL(:, 1:end-x, :) = G(:, 1+x:end, :);
    shiftedL = uint8(shiftedL)
end